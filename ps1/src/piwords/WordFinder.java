package piwords;

import java.util.HashMap;
import java.util.Map;

public class WordFinder {
    /**
     * Given a String (the haystack) and an array of Strings (the needles),
     * return a Map<String, Integer>, where keys in the map correspond to
     * elements of needles that were found as substrings of haystack, and the
     * value for each key is the lowest index of haystack at which that needle
     * was found. A needle that was not found in the haystack should not be
     * returned in the output map.
     * 
     * @param haystack The string to search into.
     * @param needles The array of strings to search for. This array is not
     *                mutated.
     * @return The list of needles that were found in the haystack.
     */
    public static Map<String, Integer> getSubstrings(String haystack,
                                                     String[] needles) { 
        // TODO: Implement (Problem 4.b)
    	int x , y, j, sub_lengthN;
    	Map<String, Integer> irereturn = new HashMap<String, Integer>();
    	
    	for( x = 0; x<needles.length; x++){
    		//System.out.println(needles.length);
    		sub_lengthN = needles[x].length();
    		//System.out.println(sub_lengthN);
    		j = 0;
    		for(y = 0; y<needles.length-sub_lengthN; y++){
    			j = haystack.indexOf(needles[x], y);
    			if(j>=0){
        			irereturn.put(needles[x], j);
        			System.out.println(j);
        			break;
        		}
    		}
    		
    	}
    	
    	return irereturn;
    }
}

