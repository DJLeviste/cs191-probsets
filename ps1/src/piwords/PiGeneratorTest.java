package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
    @Test
    public void basicPowerModTest() {
        // 5^7 mod 23 = 17
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(25, PiGenerator.powerMod(9,8,56));
        assertEquals(-1, PiGenerator.powerMod(9,8,0));
        assertEquals(0, PiGenerator.powerMod(1,0,1));
    }
    
    public void basicComputePiInHex(){
    	int output1[] = {2, 4, 3, 15, 6, 10, 8, 8, 8, 5};
    	assertArrayEquals(output1, PiGenerator.computePiInHex(10));
    	int output2[] = {2, 4, 3, 15, 6, 10, 8, 8, 8};
    	assertArrayEquals(output2, PiGenerator.computePiInHex(9));
    	int output3[] = {2, 4, 3, 15, 6, 10, 8, 8, 8, 5, 10, 3, 0, 8, 13};
    	assertArrayEquals(output3, PiGenerator.computePiInHex(9));

    	
    	//assertArrayEquals(output2, PiGenerator.computePiInHex(9));
    }

    // TODO: Write more tests (Problem 1.a, 1.c)
}
