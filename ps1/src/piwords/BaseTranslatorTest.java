package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class BaseTranslatorTest {
    @Test
    public void basicBaseTranslatorTest() {
        // Expect that .01 in base-2 is .25 in base-10
        // (0 * 1/2^1 + 1 * 1/2^2 = .25)
        int[] input = {0, 1}, inp2 = {3,1,4}, inp3 = {4,6};
        int[] expectedOutput = {2, 5},out2 = {6,7,2}, out3 = {5,9};
        assertArrayEquals(expectedOutput,BaseTranslator.convertBase(input, 2, 10, 2));
        assertArrayEquals(out2,BaseTranslator.convertBase(inp2,5, 10, 3));
        assertArrayEquals(out3,BaseTranslator.convertBase(inp3,8, 10, 2));
    }

    // TODO: Write more tests (Problem 2.a)
}
