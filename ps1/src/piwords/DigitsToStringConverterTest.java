package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        // Input is a 4 digit number, 0.123 represented in base 4
        int[] input = {0, 1, 2, 3}, input2 = {4,3,0,1,2,5};
        // Want to map 0 -> "d", 1 -> "c", 2 -> "b", 3 -> "a"
        char[] alphabet = {'d', 'c', 'b', 'a'}, alphabet2 = {'c', 'o', 'l', 'i', 'n','e'};
        String expectedOutput = "dcba", expect2 = "nicole";
        assertEquals(expectedOutput,DigitsToStringConverter.convertDigitsToString(input, 4, alphabet));
        assertEquals(null,DigitsToStringConverter.convertDigitsToString(input, 3, alphabet));
        assertEquals(expect2,DigitsToStringConverter.convertDigitsToString(input2, 6, alphabet2));
        assertEquals(null,DigitsToStringConverter.convertDigitsToString(input2, input.length, alphabet2));
        
    }

    // TODO: Write more tests (Problem 3.a)
}
